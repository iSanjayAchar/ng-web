import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'onboarding', loadChildren: './modules/onboarding.module#OnboardingModule' },
  { path: 'account', loadChildren: './modules/account.module#AccountModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
