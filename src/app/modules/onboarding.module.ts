import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule} from '@angular/forms'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2Webstorage } from 'ngx-webstorage';

import { SharedModule } from './shared.module';

import { HomeComponent } from '../components/onboarding/home/home.component';
import { VerificationComponent } from '../components/onboarding/verification/verification.component';
import { LocationComponent } from '../components/onboarding/location/location.component';
import { OutletComponent } from '../components/onboarding/outlet/outlet.component';

import { HttpService } from '../services/http.service';
import { FinalComponent } from '../components/onboarding/final/final.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'verification', component: VerificationComponent },
  { path: 'address', component: LocationComponent },
  { path: 'outlet', component: OutletComponent },
  { path: 'creating-account', component: FinalComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    Ng2Webstorage.forRoot({
      prefix: '',
      separator: ''
    }),
    SharedModule
  ],
  declarations: [
    HomeComponent, 
    VerificationComponent, 
    LocationComponent, 
    OutletComponent, 
    FinalComponent
  ],
  providers: [
    HttpService
  ]
})
export class OnboardingModule { }
