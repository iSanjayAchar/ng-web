import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { LocalStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';

import { HttpService } from '../../../services/http.service';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {

  isLoading: boolean = false;
  locationForm: FormGroup;
  error: string;

  constructor(private formBuilder: FormBuilder, private http: HttpService, private localStorage: LocalStorageService, private route: Router) {

    this.locationForm = formBuilder.group({
      "address" : [null, Validators.required],
      "pincode" : [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(7)])],
      "city" : [null, Validators.required],
      "country" : ['India', Validators.required],
      "token" : [this.localStorage.retrieve('_token'), Validators.required]
    }) 

   }

  ngOnInit() {
  }

  submit(data) {
    this.http.post(environment.endpoint + '/location', data)
        .subscribe(resp => {
          if (resp['status'] == 200) {
            this.route.navigate(['/onboarding/outlet'])
          }
        }, err => {

          switch(err['status']) {
            case 401: {
              this.localStorage.clear();
              this.route.navigate(['/'])
            }

            case 404: {
              this.error = "Whoops, something went wrong. Please contact support"
            }

          }
        })
  }

}
