import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { LocalStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';

import { HttpService } from '../../../services/http.service';

@Component({
  selector: 'app-outlet',
  templateUrl: './outlet.component.html',
  styleUrls: ['./outlet.component.scss']
})
export class OutletComponent implements OnInit {

  isLoading: boolean = false;
  outletForm: FormGroup;
  error: string;


  constructor(private formBuilder: FormBuilder, private http: HttpService, private localStorage: LocalStorageService, private route: Router) {

    this.outletForm = formBuilder.group({
      'name': [null, Validators.required],
      'code': [null, Validators.compose([Validators.required, Validators.minLength(7), Validators.maxLength(7)])],
      'legalName': [null, Validators.required],
      'address': formBuilder.group({
        'address': [null, Validators.required],
        'pincode': [null, Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(7)])],
        'city': [null, Validators.required],
        'state': [null, Validators.required],
        'country': ['India', Validators.required]
      }),
      'manager': formBuilder.group({
        'name': [null, Validators.required],
        'phone': [null, Validators.compose([Validators.required, Validators.minLength(7)])],
        'email': [null, Validators.required]
      }),
      'contact': formBuilder.group({
        'phone': [null, Validators.compose([Validators.required, Validators.minLength(7)])],
        'email': [null, Validators.required]
      }),
      'gst': [null, Validators.compose([Validators.minLength(15), Validators.maxLength(15)])]
    })
  }

  ngOnInit() {
  }

  submit(data) {
    let body = {
      token: this.localStorage.retrieve('_token'),
      outlet: data
    }
    this.http.post(environment.endpoint + '/outlet', body)
        .subscribe(resp => {
          if (resp['status'] == 200) {
            this.route.navigate(['/onboarding/creating-account'])
          }
        }, err => {
          switch(err['status']) {
            case 401: {
              this.localStorage.clear();
              this.route.navigate(['/'])
            }

            case 404: {
              this.error = "Whops, something went wrong while creating the account. Please contact support"
            }

          }          
        })
  }

}
