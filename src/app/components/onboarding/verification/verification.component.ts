import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../services/http.service';
import { environment } from '../../../../environments/environment';
import { LocalStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.scss']
})

export class VerificationComponent implements OnInit {

  seconds: number = 30;
  count: any = setInterval(() => { this.seconds > 0 ? this.seconds -= 1 : clearInterval(this.count)  }, 1000);  
  invalid: boolean; 
  resent: boolean = false;
  error: string = null;
  code: number;


  constructor(private http: HttpService, private localStorage: LocalStorageService, public router: Router) {
 
  }

  ngOnInit() {

  }

  resend() {
    if (this.seconds == 0) {

      let body = {
        token: this.localStorage.retrieve('_token')
      }

      this.http.post(environment.endpoint + '/resend-otp', body)
          .subscribe(resp => {
            if (resp['status'] == 200) {
              this.resent = true;
            }
          }, err => {
            this.error = "Whoops, something went wrong"
          })
    }
  }

  verify(code) {
    let body = {
      token: this.localStorage.retrieve('_token'),
      secret: code
    }

    this.http.post(environment.endpoint + '/verification', body)
        .subscribe(resp => {
          if (resp['status'] == 200) {
            this.router.navigate(['/onboarding/address'])
          }
        }, err => {
          console.log(err['status'])
          switch(err['status']) {
            case 404: {
              this.localStorage.clear();
              this.router.navigate(['/']);
              break;
            } 
            
            case 500: {
              this.error = "Whoops, something is wrong. We're fixing it";
              break;
            }

            case 403: {
              this.error = "You've entered an invalid code";
              break;
            }

            default: {
              this.error = "Something went wrong, please try again";
              break;
            }
          }
        })
  }

}
