import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { LocalStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { HttpService } from '../../../services/http.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  isLoading: boolean = false;
  onboardingForm: FormGroup;
  form: any = {
    name: '',
    legalName: '',
    contact: {
      name: '',
      phone: null,
      email: ''
    }
  }
  modal: any;

  constructor(private modalService: NgbModal, private formBuilder: FormBuilder, private http: HttpService, private localStorage: LocalStorageService, private route: Router) { 

    this.onboardingForm = formBuilder.group({
      'name' : [null, Validators.required],
      'legalName' : [null, Validators.required],
      'contact' : formBuilder.group({
        'name' : [null, Validators.required],
        'email' : [null, Validators.required],
        'phone' : [null, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])]
      }),
      'toc': [true, Validators.required]
    })
  }

  ngOnInit() {
    console.log(this.modal);
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      console.log(result)
    }, (reason) => {
      console.log(reason);
    });       
  }  

  submit(data, modal) {
    this.isLoading = true;
    this.http.post(environment.endpoint + '/business', data)
        .subscribe(resp => {
          this.localStorage.store('_token', resp['message'].token);
          this.route.navigate(['/onboarding/verification']);
        }, err => {
          this.isLoading = false;
          if (err['status'] == 409) {
            this.open(modal)
          }
        })
  }

}
